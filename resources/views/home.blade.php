@extends('layouts.app')

@section('content')
    <div id="app">

    </div>
@endsection
@push('js')
    <script>
        window.user={
            id:"{{auth()->id()}}",
            name:"{{auth()->user()->name}}",
        };
        window.csrfToken="{{csrf_token()}}";
    </script>
@endpush
